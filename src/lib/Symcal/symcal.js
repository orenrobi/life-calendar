// @TODO watch
// https://docs.npmjs.com/getting-started/creating-node-modules
// read
// https://www.terlici.com/2014/08/25/best-practices-express-structure.html
// http://www.innofied.com/node-js-best-practices/

// Extend Date? https://stackoverflow.com/questions/6075231/how-to-extend-the-javascript-date-object
//class SymDate {
  // ...
//}
//module.exports = { SymDate }

import isostring from 'isostring';
import { SearchSunLongitude } from 'astronomy-engine';
//

function getZodiacs(year) {
  function find(targetLon, month, day) {
    let startDate = new Date(Date.UTC(year, month - 1, day));
    let time = SearchSunLongitude(targetLon, startDate, 6);
    if (!time)
      throw `Cannot find season change near ${startDate.toISOString()}`;
    return time;
  }
  if ((year instanceof Date) && Number.isFinite(year.getTime()))
    year = year.getUTCFullYear();
  if (!Number.isSafeInteger(year))
    throw `Cannot calculate seasons because year argument ${year} is neither a Date nor a safe integer.`;
    //let mar_equinox = find(0, 3, 19);
    //let jun_solstice = find(90, 6, 19);
    //let sep_equinox = find(180, 9, 21);
    //let dec_solstice = find(270, 12, 20);
    return {
      aqu: find(300, 1, 17),
      pis: find(330, 2, 16),
      ari: find(0, 3, 19),
      tau: find(30, 4, 17),
      gem: find(60, 5, 18),
      can: find(90, 6, 19),
      leo: find(120, 7, 20),
      vir: find(150, 8, 21),
      lib: find(180, 9, 21),
      sco: find(210, 10, 21),
      sag: find(240, 11, 20),
      cap: find(270, 12, 20)
    };
  }

  const helpers = {
    quotient: function (x, y) {
      return Math.floor(x / y);
    },
    mod: function (x, y) {
      return x - (y * this.quotient(x, y));
    },
    amod: function (x, y) {
      return y + this.mod(x, -1 * y);
    },
    weekdayNames: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
    monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
    getWeekdayName: function () {},
    getMonthName: function () {},
    getWeekdayAbbr: function (n) {
      if (n <= 7 && n >= 1) {
        return this.weekdayNames[n - 1].substring(0, 3);
      }
      return 'Und';
    },
    getMonthAbbr: function (n) {
      if (n <= 12 && n >= 1) {
        return this.monthNames[n - 1].substring(0, 3);
      }
      return 'Und';
    },
    getOrdinalSuffix: function (number) {
      if (number > 3 && number < 21) return 'th';
      switch (number % 10) {
        case 1:
        return 'st';
        case 2:
        return 'nd';
        case 3:
        return 'rd';
        default:
        return 'th';
      }
    }
  };

// Derivation: 1000 * 60 * 60 * 24
const DAY_MILLISECONDS = 86400000;
// Length of a leap cycle in the standard Symmetry454 calendar system.
const CYCLE_YEARS = 293;
// The number of leap years during one leap cycle in the Symmetry454 system.
const CYCLE_LEAPS = 52;
// Used to determine whether a year is leap. Derivation: (CYCLE_YEARS - 1) / 2
const LEAP_COEFFICIENT = 146;
// Derivation: ((CYCLE_YEARS * 52) + CYCLE_LEAPS))  * 7) / CYCLE_YEARS
const MEAN_YEAR = 365.24232081911265;

Date.prototype.getDayNum = function () {
  var onejan = new Date(this.getFullYear(), 0, 1);
  return Math.ceil((this - onejan) / DAY_MILLISECONDS);
};

var symcal = {};

// Previous attempt:
// https://stackoverflow.com/questions/5670678/javascript-coding-input-a-specific-date-output-the-season
symcal.getSeasons = function (y) {
  return getZodiacs(y);
};


Math.degRad = function (d) {
  return (d * Math.PI) / 180.0;
};
Math.degSin = function (d) {
  return Math.sin(Math.degRad(d));
};
Math.degCos = function (d) {
  return Math.cos(Math.degRad(d));
};

symcal.quotient = function (x, y) {
  return this.floor(x / y);
};
symcal.mod = function (x, y) {
  return x - (y * symcal.quotient(x, y));
};
symcal.floor = function (x) {
  return Math.floor(x);
};
symcal.isSymDate = function (symDate) {
  if (typeof symDate.year == 'undefined' || !Number.isInteger(Number(symDate.year))) {
    return false;
  }
  if (typeof symDate.dayOfYear == 'undefined' || !Number.isInteger(Number(symDate.dayOfYear))) {
    return false;
  }
  return true;
};

symcal.isSymLeapYear = function (symYear) {
  var dividend = (CYCLE_LEAPS * symYear) + LEAP_COEFFICIENT;
  var accumulator = symcal.mod(dividend, CYCLE_YEARS);
  return accumulator < CYCLE_LEAPS;
};

/**
* Returns the fixed date of Jan 1 of the given symmetrical year.
*/
symcal.symNewYearDay = function (symYear) {
  var priorYear = symYear - 1;
  var shortTotal = (7 * 52 * priorYear) + 1;
  var leapTotal = symcal.floor(((CYCLE_LEAPS * priorYear) + LEAP_COEFFICIENT) / CYCLE_YEARS);
  return (7 * leapTotal) + shortTotal;
};

symcal.gregYearLength = function (gregYear) {
  var length = 365;
  if ((symcal.mod(gregYear, 4) == 0 && symcal.mod(gregYear, 100) != 0)
    || (symcal.mod(gregYear, 400) == 0)) {
    length++;
  length++;
}
return length;
};
symcal.gregYearLength = function (gregYear) {
  var length = 365;
  if (symcal.mod(gregYear, 4) == 0 && symcal.mod(gregYear, 100) != 0) {
    length++;
  }
  else if (symcal.mod(gregYear, 400) == 0) {
    length++;
  }
  return length;
};
symcal.cleanSource = function (input) {
  // @TODO Force noon UTC
  if (input instanceof Date) {
    return input.toISOString();
  }
  if (isostring(input)) {
    return (new Date(input)).toISOString();
  }
  if (symcal.isSymDate(input)) {
    return symcal.cleanSymDate(input);
  }
  return false;
};

symcal.cleanSymDate = function (symDate) {
  return {
    year: Number(symDate.year),
    dayOfYear: Number(symDate.dayOfYear)
  };
};

symcal.convert = function (input, format, distinctFormatting) {
  //console.log(input);
  var source = symcal.cleanSource(input);
  if (source === false) {
    return false;
  }
  var format = format || 'short';
  if (isostring(source)) {
    return symcal.ISOStringToSym(new Date(source), format);
  }
  return symcal.symToISOString(source, format);
};

symcal.ISOStringToSym = function (d, format) {
  var gregDate = {
    year: d.getFullYear(),
    dayOfYear: d.getDayNum()
  };
  var fixedDate = symcal.priorElapsedDays(gregDate.year) + gregDate.dayOfYear;
  var symDate = {
    year: symcal.fixedToSymYear(fixedDate)
  };
  symDate.dayOfYear = fixedDate - symcal.symNewYearDay(symDate.year) + 1;
  symDate = symcal.expandSymDate(symDate);
  return symcal.formatSym(symDate, format);
};

symcal.bootStrapSymDate = function(fixedDate) {
  var symDate = {
    year: symcal.fixedToSymYear(fixedDate)
  };
  symDate.dayOfYear = fixedDate - symcal.symNewYearDay(symDate.year) + 1;
  symDate = symcal.expandSymDate(symDate);
  return symDate;
};

symcal.bootStrapSymDateFromYearAndDay = function (sYear, dayOfYear) {
  return symcal.expandSymDate({
    year: sYear,
    dayOfYear: dayOfYear
  });
}

symcal.gregSymNYD = function(year) {
  var symDate = symcal.symDateFromYear(year);
  return symcal.expandSymDate(symDate);//symcal.symToISOString(symDate);
};
symcal.symDateFromYear = function(year) {
  var newYearDay = symcal.symNewYearDay(year);
  return symcal.bootStrapSymDate(newYearDay);
}

symcal.priorElapsedDays = function (gregYear) {
  var priorYear = gregYear - 1;
  var days = (priorYear * 365) + 1;
  days += symcal.floor(priorYear / 4);
  days -= symcal.floor(priorYear / 100);
  days += symcal.floor(priorYear / 400);
  return days;
};

symcal.shiftGreg = function (isNegativeYear, gregDate) {
  if (isNegativeYear) {
    return {
      year: gregDate.year - 1,
      dayOfYear: gregDate.dayOfYear + symcal.gregYearLength(gregDate.year)
    };
  }
  return {
    year: gregDate.year + 1,
    dayOfYear: gregDate.dayOfYear - symcal.gregYearLength(gregDate.year)
  };
};

symcal.isResolvedGreg = function (isNegativeYear, gregDate) {
  if (isNegativeYear) {
    return gredDate.dayOfYear >= 0;
  }
  return gregDate.dayOfYear < symcal.gregYearLength(gregDate.year);
};

symcal.symToISOString = function (symDate, format) {
  var newYearDay = symcal.symNewYearDay(symDate.year);
  var fixedDate = newYearDay + symDate.dayOfYear - 1;
  var isNegativeYear = (fixedDate < 365);
  var gregDate = {
    year: 1,
    dayOfYear: fixedDate
  };
  while (!symcal.isResolvedGreg(isNegativeYear, gregDate)) {
    gregDate = symcal.shiftGreg(isNegativeYear, gregDate);
  }
  var d = (new Date(gregDate.year, 0)).setDate(gregDate.dayOfYear);
  // @TODO Force noon UTC
  return symcal.formatISOString(d, format);
};

symcal.formatISOString = function (date, format) {
  return new Date(date).toISOString();
};

symcal.fixedToSymYear = function (fixedDate) {
  var symYear = Math.ceil((fixedDate - 1) / MEAN_YEAR);
  var newYearDay = symcal.symNewYearDay(symYear);
  if (newYearDay < fixedDate) {
    if ((fixedDate - newYearDay) >= (7 * 52)
      && (fixedDate >= symcal.symNewYearDay(symYear + 1))) {
      symYear++;
  }
}
else if (newYearDay > fixedDate) {
  symYear--;
}
return symYear;
};
symcal.fixedToSym = function (fixedDate) {
  var symYear = symcal.fixedToSymYear(fixedDate);
  var startOfYear = symcal.symNewYearDay(symYear);
  var dayOfYear = fixedDate - startOfYear + 1;
  return {
    year: symYear,
    dayOfYear: dayOfYear
  };
};

symcal.symDaysInMonth = function (symDate) {
  if ((symDate.monthOfQuarter == 2)
    || (symDate.monthOfQuarter == 3 && symDate.quarter == 4 && symDate.isLeap)) {
    return 35;
}
return 28;
};

/**
 * Symmetrical (Distinct formatting)
 * Micro    1999/12/5/7
 * Short    1999/12/5/Sun
 * Standard 1999 Dec 5th Sun
 * Medium   5th Sunday, Dec 1999
 * Long     5th Sunday of December 1999
 */
 symcal.formatSym = function (symDate, format) {
  var formatted = false;
  switch (format) {
    case 'micro':
    formatted = [symDate.year, symDate.monthOfYear, symDate.weekOfMonth, symDate.dayOfWeek].join('/');
    break;
    case 'short':
    formatted = [symDate.year, symDate.monthShort, symDate.weekOfMonth, symDate.dayOfWeekShort].join('/');
    break;
    case 'standard':
    formatted = [symDate.year, symDate.monthShort, symDate.weekOfMonth + symDate.weekOfMonthSuffix, symDate.dayOfWeekShort].join(' ');
    break;
    case 'medium':
    formatted = [symDate.weekOfMonth + symDate.weekOfMonthSuffix, symDate.dayOfWeekLong + ',', symDate.monthShort, symDate.year].join(' ');
    break;
    case 'long':
    formatted = [symDate.weekOfMonth + symDate.weekOfMonthSuffix, symDate.dayOfWeekLong, 'of', symDate.monthLong, symDate.year].join(' ');
    break;
  }

  return formatted;
};

symcal.getZodiac = function(sD) {
  var result = "";
  var zodiacs = ['aqu', 'pis', 'ari', 'tau', 'gem', 'can', 'leo', 'vir', 'lib', 'sco', 'sag', 'cap'];
  var seasons = (sD.seasons.length) ? sD.seasons : symcal.getSeasons(sD.greg.year);
  var today = sD.greg.date;
  for (var i = 0; i < zodiacs.length; i++) {
    if (i == 0) {
      var compareDateA = sD.NYDgreg.date;
      var compareDateB = seasons[zodiacs[i]].date;
      result = zodiacs[zodiacs.length - 1];
    }
    else {
      var compareDateA = seasons[zodiacs[i]].date;
      if (i + 1 < zodiacs.length) {
        var compareDateB = seasons[zodiacs[i + 1]].date;
      }
      else {
        var compareDateB = sD.nextNYDgreg.date;
      }
      result = zodiacs[i];
    }
    /*
    console.log({
      i: i,
      compA: compareDateA,
      today: today,
      compB: compareDateB,
      isBetween: (today >= compareDateA && today < compareDateB),
      result: result,
      seasons: seasons
    });*/
    if (today >= compareDateA && today < compareDateB) {
      break;
    }
  }

  return result;
}
symcal.getSeason = function(sD) {
  var seasons = {
    aqu: 'w',
    pis: 'w',
    ari: 'p',
    tau: 'p',
    gem: 'p',
    can: 's',
    leo: 's',
    vir: 's',
    lib: 'f',
    sco: 'f',
    sag: 'f',
    cap: 'w'
  };
  return seasons[sD.zodiac];
}

symcal.expandSymDate = function (symDate) {
  symDate.NYD = symcal.symNewYearDay(symDate.year);
  symDate.NYDgreg = symcal.getGregorian({fixedDate: symDate.NYD});
  symDate.nextNYD = symcal.symNewYearDay(symDate.year + 1);
  symDate.nextNYDgreg = symcal.getGregorian({fixedDate: symDate.nextNYD});
  symDate.fixedDate = symDate.NYD + symDate.dayOfYear - 1;
  symDate.zodiac = symcal.getZodiac(symDate);
  symDate.season = symcal.getSeason(symDate);
  // D.y.week
  symDate.yearWeek = Math.ceil(symDate.dayOfYear / 7);

  // D.quarter
  symDate.quarter = Math.ceil((4 / 53) * symDate.yearWeek);
  // D.q.day
  symDate.dayOfQuarter = symDate.dayOfYear - (13 * 7 * (symDate.quarter - 1));
  // D.q.week
  symDate.weekOfQuarter = Math.ceil(symDate.dayOfQuarter / 7);
  // D.q.month
  symDate.monthOfQuarter = Math.min(3, Math.ceil((2 / 9) * symDate.weekOfQuarter));

  // D.isLeap
  symDate.isLeap = symcal.isSymLeapYear(symDate.year);
  // D.m.days
  symDate.daysInMonth = symcal.symDaysInMonth(symDate);
  // D.month
  symDate.monthOfYear = 3 * (symDate.quarter - 1) + symDate.monthOfQuarter;
  // D.m.abbr
  symDate.monthShort = helpers.getMonthAbbr(symDate.monthOfYear);
  // D.m.name
  symDate.monthLong = helpers.monthNames[symDate.monthOfYear - 1];
  // D.m.day
  symDate.dayOfMonth = symDate.dayOfYear - ((28 * (symDate.monthOfYear - 1)) + (7 * symcal.quotient(symDate.monthOfYear, 3)));
  // D.m.d.abbr
  symDate.dayOfMonthSuffix = helpers.getOrdinalSuffix(symDate.dayOfMonth);
  // D.week
  symDate.weekOfMonth = Math.ceil(symDate.dayOfMonth / 7);
  // D.w.name
  symDate.weekOfMonthSuffix = helpers.getOrdinalSuffix(symDate.weekOfMonth);
  // D.day
  symDate.dayOfWeek = symcal.mod(symDate.dayOfYear - 1, 7) + 1;
  // D.d.abbr
  symDate.dayOfWeekShort = helpers.getWeekdayAbbr(symDate.dayOfWeek);
  // D.d.name
  symDate.dayOfWeekLong = helpers.weekdayNames[symDate.dayOfWeek - 1];

  /*
  var D = {
    isLeap: false,
    year: 2015,
    quarter: 1,
    month: 1,
    week: 1,
    day: 1,
    y: {
      week: 1,
    },
    q: {
      month: 1,
      week: 1,
      day: 1,
    },
    m: {
      name: "January",
      abbr: "Jan",
      day: 1
      d: {
        abbr: "1st"
      }
    },
    w: {
      name: "1st"
    },
    d: {
      name: "Monday",
      abbr: "Mon"
    }
  };

  */

  symDate.micro = symcal.formatSym(symDate, 'micro');
  symDate.short = symcal.formatSym(symDate, 'short');
  symDate.standard = symcal.formatSym(symDate, 'standard');
  symDate.medium = symcal.formatSym(symDate, 'medium');
  symDate.long = symcal.formatSym(symDate, 'long');
  symDate.daysInYear = (symDate.isLeap) ? (7 * 53) : (7 * 52);

  return symDate;
};

symcal.isNormalInteger = function (str) {
  var n = Math.floor(Number(str));
  return n !== Infinity && String(n) === str && n >= 0;
}

symcal.getGregFull = function (d) {
  let wd = new Intl.DateTimeFormat('en', { weekday: 'long' }).format(d);
  let ye = new Intl.DateTimeFormat('en', { year: 'numeric' }).format(d);
  let mo = new Intl.DateTimeFormat('en', { month: 'short' }).format(d);
  let da = new Intl.DateTimeFormat('en', { day: 'numeric' }).format(d);
  return `${wd}, ${mo} ${da}, ${ye}`;
}
/**
 * Input: {fixedDate: [int]}
 */
 symcal.getGregorian = function (sD) {
  var isNegativeYear = (sD.fixedDate < 365);
  var greg = {
    year: 1,
    dayOfYear: sD.fixedDate
  };
  while (!symcal.isResolvedGreg(isNegativeYear, greg)) {
    greg = symcal.shiftGreg(isNegativeYear, greg);
  }
  var d = new Date(greg.year, 0);
  d.setDate(greg.dayOfYear);
  greg.date = d;
  greg.full = symcal.getGregFull(greg.date);
  return greg;
}
symcal.getLeapInfo = function (sD) {
  sD.isLeap = symcal.isSymLeapYear(sD.year);
  sD.daysInYear = (sD.isLeap) ? (7 * 53) : (7 * 52);
  return sD;
}
symcal.getGregorianFixed = function (sD) {
  sD.year = symcal.fixedToSymYear(sD.fixedDate);
  sD = symcal.getLeapInfo(sD);
  sD.NYD = symcal.symNewYearDay(sD.year);
  sD.dayOfYear = sD.fixedDate - sD.NYD + 1;
  sD.greg = symcal.getGregorian(sD);
  sD.seasons = symcal.seasonsForYear(sD.greg.date.getFullYear());
  return sD;
};
symcal.getGregorianMod = function(sD) {
  // Check for out of bounds & shift
  sD = symcal.getLeapInfo(sD);
  sD.NYD = symcal.symNewYearDay(sD.year);
  sD.fixedDate = sD.NYD + sD.dayOfYear - 1;
  sD.greg = symcal.getGregorian(sD);
  sD.seasons = symcal.seasonsForYear(sD.greg.date.getFullYear());
  return sD;
}
/**
 * Must fill in all:
 * Input: {
 *  greg: {date: [object Date]}
 * }
 *
 * Output: {
 *   fixedDate
 *   year
 *   dayOfYear
 *   isLeap
 *   daysInYear
 *   NYD
 *   seasons
 *   greg {
 *     year:
 *     dayOfYear:
 *     date:
 *     full:
 *   }
 * }
 */
 symcal.expandGregorian = function (sD) {
  console.log("expanding gregorian...");
  console.log({init: sD});
  var gregYear = sD.greg.date.getFullYear();
  sD.fixedDate = symcal.priorElapsedDays(gregYear) + sD.greg.date.getDayNum();
  sD.year = symcal.fixedToSymYear(sD.fixedDate);
  sD.NYD = symcal.symNewYearDay(sD.year);
  sD.seasons = symcal.seasonsForYear(gregYear);
  sD.dayOfYear = sD.fixedDate - sD.NYD + 1;
  sD = symcal.getLeapInfo(sD);
  sD.greg = symcal.getGregorian(sD);
  sD = symcal.expandSymDate(sD);
  console.log({post: sD});
  return sD;
}
symcal.expandSymmetricalMod = function (sD) {
  sD = symcal.getGregorianMod(sD);
  return symcal.expandSymDate(sD);
}
symcal.expandSymmetricalFixed = function (sD) {
  console.log("expanding fixed date...");
  sD = symcal.getGregorianFixed(sD);
  console.log(sD);
  return sD;
}

symcal.from = function (var1, var2) {
  var sD = {process: false};
  if (Object.prototype.toString.call(var1) === '[object Date]') {
    sD.greg = {date: var1};
    sD.process = 'expandGregorian';
  }
  else {
    // Convert strings into integers
    if (!Number.isInteger(var1) && symcal.isNormalInteger(var1)) {
      var1 = Math.floor(Number(var1));
    }
    if (!Number.isInteger(var2) && symcal.isNormalInteger(var2)) {
      var2 = Math.floor(Number(var2));
    }
    if (Number.isInteger(var1)) {
      if (Number.isInteger(var2)) {
        sD.year = var1;
        sD.dayOfYear = var2;
        sD.process = 'expandSymmetricalMod';
      }
      else {
        sD.fixedDate = var1;
        sD.process = 'expandSymmetricalFixed';
      }
    }
  }
  if (sD.process) {
    return symcal[sD.process](sD);
  }
  return false;
}

symcal.seasonsForYear = function (y) {
  return symcal.getSeasons(y);
}

function SymYear(obj) {
  if (obj instanceof SymYear) return obj;
  if (!(this instanceof SymYear)) return new SymYear(obj);
  var props = obj;
  for (var name in props) {
    if (props.hasOwnProperty(name)) {
        // here you could implement type checking exploiting props[name]
        this[name] = obj[name];
      }
    }
  }



  export default symcal;
