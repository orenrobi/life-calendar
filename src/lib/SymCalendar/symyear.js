/* TODO */
// Chinese new year dates
// https://github.com/commenthol/date-chinese#chinese-new-year
// Chinese zodiac animals for dates:
// https://www.wikiwand.com/en/Chinese_New_Year#/Dates_in_Chinese_lunisolar_calendar
// Lunar cycle (New, Full)

import { SearchSunLongitude } from 'astronomy-engine';
// Derivation: 1000 * 60 * 60 * 24
const DAY_MILLISECONDS = 86400000;
// Length of a leap cycle in the standard Symmetry454 calendar system.
const CYCLE_YEARS = 293;
// The number of leap years during one leap cycle in the Symmetry454 system.
const CYCLE_LEAPS = 52;
// Used to determine whether a year is leap. Derivation: (CYCLE_YEARS - 1) / 2
const LEAP_COEFFICIENT = 146;
// Derivation: ((CYCLE_YEARS * 52) + CYCLE_LEAPS))  * 7) / CYCLE_YEARS
const MEAN_YEAR = 365.24232081911265;
// Derivation: 7 * 53
const LEAP_YEAR_DAYS = 371;

Date.prototype.getDayNum = function () {
  var onejan = new Date(this.getFullYear(), 0, 1);
  return Math.ceil((this - onejan) / DAY_MILLISECONDS);
};

const helpers = {};
helpers.quotient = function (x, y) {
  return Math.floor(x / y);
};
helpers.mod = function (x, y) {
  return x - (y * helpers.quotient(x, y));
};
helpers.fixedNydForYear = function(y) {
  var priorYear = y - 1;
  var shortTotal = (7 * 52 * priorYear) + 1;
  var leapTotal = Math.floor(((CYCLE_LEAPS * priorYear) + LEAP_COEFFICIENT) / CYCLE_YEARS);
  return (7 * leapTotal) + shortTotal;
};
helpers.isLeapYear = function (year) {
  var dividend = (CYCLE_LEAPS * year) + LEAP_COEFFICIENT;
  var accumulator = helpers.mod(dividend, CYCLE_YEARS);
  return accumulator < CYCLE_LEAPS;
};
helpers.isResolvedGreg = function (isNegativeYear, gregDate) {
  if (isNegativeYear) {
    return gredDate.dayOfYear >= 0;
  }
  return gregDate.dayOfYear < helpers.gregYearLength(gregDate.year);
};
helpers.gregYearLength = function (gregYear) {
  var length = 365;
  if (helpers.mod(gregYear, 4) == 0 && helpers.mod(gregYear, 100) != 0) {
    length++;
  }
  else if (helpers.mod(gregYear, 400) == 0) {
    length++;
  }
  return length;
};
helpers.shiftGreg = function(isNegativeYear, gregDate) {
  if (isNegativeYear) {
    return {
      year: gregDate.year - 1,
      dayOfYear: gregDate.dayOfYear + helpers.gregYearLength(gregDate.year)
    };
  }
  return {
    year: gregDate.year + 1,
    dayOfYear: gregDate.dayOfYear - helpers.gregYearLength(gregDate.year)
  };
};
helpers.dateFromGreg = function(greg) {
  var d = new Date(greg.year, 0);
  d.setDate(greg.dayOfYear);
  return d;
}
helpers.dateFromFixed = function(fixedDate) {
  var isNegativeYear = (fixedDate < 365);
  var greg = {
    year: 1,
    dayOfYear: fixedDate
  };
  while (!helpers.isResolvedGreg(isNegativeYear, greg)) {
    greg = helpers.shiftGreg(isNegativeYear, greg);
  }
  return helpers.dateFromGreg(greg);
}
helpers.getSolarEvents = function(year) {
  function find(targetLon, month, day) {
    let startDate = new Date(Date.UTC(year, month - 1, day));
    let time = SearchSunLongitude(targetLon, startDate, 6);
    if (!time)
      throw `Cannot find season change near ${startDate.toISOString()}`;
    return time;
  }
  if ((year instanceof Date) && Number.isFinite(year.getTime()))
    year = year.getUTCFullYear();
  if (!Number.isSafeInteger(year))
    throw `Cannot calculate seasons because year argument ${year} is neither a Date nor a safe integer.`;
  return {
    aqu: find(300, 1, 17),
    pis: find(330, 2, 16),
    ari: find(0, 3, 19),
    tau: find(30, 4, 17),
    gem: find(60, 5, 18),
    can: find(90, 6, 19),
    leo: find(120, 7, 20),
    vir: find(150, 8, 21),
    lib: find(180, 9, 21),
    sco: find(210, 10, 21),
    sag: find(240, 11, 20),
    cap: find(270, 12, 20)
  };
}
helpers.priorElapsedDays = function (gregYear) {
  var priorYear = gregYear - 1;
  var days = (priorYear * 365) + 1;
  days += Math.floor(priorYear / 4);
  days -= Math.floor(priorYear / 100);
  days += Math.floor(priorYear / 400);
  return days;
};
helpers.fixedFromDate = function (date) {
  return helpers.priorElapsedDays(date.getFullYear()) + date.getDayNum();
}
helpers.fixedToSymYear = function (fixedDate) {
  var symYear = Math.ceil((fixedDate - 1) / MEAN_YEAR);
  var newYearDay = helpers.fixedNydForYear(symYear);
  if (newYearDay < fixedDate) {
    if ((fixedDate - newYearDay) >= (7 * 52)
      && (fixedDate >= helpers.fixedNydForYear(symYear + 1))) {
      symYear++;
  }
}
else if (newYearDay > fixedDate) {
  symYear--;
}
return symYear;
};
export function SymDay(obj) {
  if (obj instanceof SymDay) return obj;
  if (!(this instanceof SymDay)) return new SymDay(obj);
  if (obj.fixedDate && Number.isInteger(obj.fixedDate)) {
    this.fixedDate = obj.fixedDate;
  }
  if (obj.dayOfYear && Number.isInteger(obj.dayOfYear)) {
    this.dayOfYear = obj.dayOfYear;
  }
  if (obj.year && Number.isInteger(obj.year)) {
    this.year = obj.year;
  }
  this.weekday = ((this.dayOfYear - 1) % 7) + 1;
  this.weekOfYear = Math.floor((this.dayOfYear - 1) / 7) + 1;
  var months = [
  1,1,1,1,2,2,2,2,2,3,3,3,3,
  4,4,4,4,5,5,5,5,5,6,6,6,6,
  7,7,7,7,8,8,8,8,8,9,9,9,9,
  10,10,10,10,11,11,11,11,11,12,12,12,12,12
  ];
  this.month = months[this.weekOfYear - 1];
  var weeks = [
  1,2,3,4,1,2,3,4,5,1,2,3,4,
  1,2,3,4,1,2,3,4,5,1,2,3,4,
  1,2,3,4,1,2,3,4,5,1,2,3,4,
  1,2,3,4,1,2,3,4,5,1,2,3,4,5
  ];
  this.week = weeks[this.weekOfYear - 1];
  var monthAbbrs = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug','Sep','Oct','Nov','Dec'];
  var weekdayAbbrs = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];
  this.short = this.year + '/' + monthAbbrs[this.month - 1] + '/' + this.week + '/' + weekdayAbbrs[this.weekday - 1];
  this.dayOfMonth = ((this.week - 1) * 7) + this.weekday;
  this.omitLeapDay = (obj.omitLeapDay === true);
  var takenCareOf = ['fixedDate', 'dayOfYear', 'year', 'weekday', 'weekOfYear', 'month', 'week', 'short', 'dayOfMonth', 'omitLeapDay'];
  this.addProps = function(obj) {
    var props = obj;
    for (var name in props) {
      if (props.hasOwnProperty(name) && !takenCareOf.includes(name)) {
        this[name] = obj[name];
      }
    }
  }
}

export function SymYear(obj) {
  if (obj instanceof SymYear) return obj;
  if (!(this instanceof SymYear)) return new SymYear(obj);
  // Add additional ways to construct?
  // {fixedDate} [int]
  // {from} [object Date]
  var process = false;
  if (obj.date && obj.date instanceof Date) process = "date";
  if (obj.year && Number.isInteger(obj.year)) process = "year";
  if (process) {
    if (process == "date") {
      this.fromDate = helpers.fixedFromDate(obj.date);
      obj.year = helpers.fixedToSymYear(this.fromDate);
    }
    this.year = obj.year;
    this.setupYearProperties = function() {
      this.nydFixed = helpers.fixedNydForYear(this.year);
      if (this.fromDate) {
        this.fromDayOfYear = this.fromDate - this.nydFixed;
      }
      this.isLeap = helpers.isLeapYear(this.year);
      this.solarEvents = helpers.getSolarEvents(this.year);
    }
    this.setupYearProperties();
    this.setupDays = function() {
      this.days = [];
      this.days.push(new SymDay({
        year: this.year,
        fixedDate: this.nydFixed,
        dayOfYear: 1
      }));
      for (var i = 2; i <= (LEAP_YEAR_DAYS + 1); i++) {
        if (i > (LEAP_YEAR_DAYS - 7) && i <= LEAP_YEAR_DAYS) {
          this.days.push(new SymDay({
            year: this.year,
            fixedDate: this.nydFixed + i - 1,
            dayOfYear: i,
            omitLeapDay: !this.isLeap
          }));
        }
        else if (i == LEAP_YEAR_DAYS + 1) {
          this.nextNydFixed = this.nydFixed + i - 1;
          this.nextNydDate = helpers.dateFromFixed(this.nextNydFixed);
        }
        else {
          this.days.push(new SymDay({
            year: this.year,
            fixedDate: this.nydFixed + i - 1,
            dayOfYear: i
          }));
        }
      }
    }
    this.setupDays();
    this.tz = obj.tz || Intl.DateTimeFormat().resolvedOptions().timeZone;
    this.populateData = function() {
      var dayOfYear = 0;
      var gregMonth = null;
      var z = 0;
      var zodiacs = ['cap','aqu','pis','ari','tau','gem','can','leo','vir','lib','sco','sag','cap','aqu'];
      var zodiacNames = ['Capricorn','Aquarius','Pisces','Aries','Taurus','Gemini','Cancer','Leo','Virgo','Libra','Scorpio','Sagittarius','Capricorn','Aquarius'];

      var s = 0;
      var seasons = ['Winter','Spring','Summer','Fall','Winter','Spring'];
      var seasonBreaks = ['cap','ari','can','lib','cap','ari'];
      let signs = {
        ari: '♈︎',
        tau: '♉︎',
        gem: '♊︎',
        can: '♋︎',
        leo: '♌︎',
        vir: '♍︎',
        lib: '♎︎',
        sco: '♏︎',
        sag: '♐︎',
        cap: '♑︎',
        aqu: '♒︎',
        pis: '♓︎'
      };
      var monthLengths = [4,5,4,4,5,4,4,5,4,4,5,5];
      var firstZodiacDay = null;
      var firstGregMonthDay = null;
      var firstSeasonDay = null;
      var text = null;
      var textClass = null;
      this.months = [];
      for (var m = 0; m < 12; m++) {
        var month = [];
        for (var w = 0; w < monthLengths[m]; w++) {
          var week = [];
          for (var d = 0; d < 7; d++) {
            var day = this.days[dayOfYear];
            var date = helpers.dateFromFixed(day.fixedDate);
            if (dayOfYear == 0) {
              gregMonth = 1;
            }
            var nextZodiac = this.solarEvents[zodiacs[z + 1]].date;
            var diffZodiac = Math.abs(nextZodiac - date);
            var diffZodiacDays = Math.ceil(diffZodiac / (DAY_MILLISECONDS));
            var nextSeason = this.solarEvents[seasonBreaks[s + 1]].date;
            var diffSeason = Math.abs(nextSeason - date);
            var diffSeasonDays = Math.ceil(diffSeason / (DAY_MILLISECONDS));
            day.addProps({
              date: date,
              zodiac: zodiacNames[z],
              //nextZodiac: nextZodiac,
              //diffZodiac: diffZodiac,
              //diffZodiacDays: diffZodiacDays,
              season: seasons[s],
              //nextSeason: nextSeason,
              //diffSeason: diffSeason,
              //diffSeasonDays: diffSeasonDays,
              gregYear: date.getUTCFullYear(),
              gregMonth: date.getMonth() + 1,
              gregDay: date.getDate(),
            });
            if (date.getMonth() == 0 && date.getDate() == 1) {
              firstGregMonthDay = 1;
            }
            if ((date.getMonth() + 1) != gregMonth) {
              gregMonth = date.getMonth() + 1;
              firstGregMonthDay = gregMonth;
            }
            if (firstGregMonthDay) {
              day.addProps({firstGregMonthDay: firstGregMonthDay});
              firstGregMonthDay = null;
            }
            if (firstZodiacDay) {
              day.addProps({firstZodiacDay: firstZodiacDay});
              if (firstSeasonDay) {
                day.addProps({firstSeasonDay: firstSeasonDay});
                firstSeasonDay = null;
              }
              firstZodiacDay = null;
            }
            text = "&nbsp;";
            textClass = "default";
            if (day.firstGregMonthDay) {
              textClass = 'firstGregMonth';
              text = day.firstGregMonthDay;
            }
            if (day.weekday == 1) {
              textClass = 'gregDay';
              text = day.gregDay;
            }
            if (day.firstZodiacDay) {
              textClass = 'firstZodiacDay';
              text = signs[day.firstZodiacDay];
            }
            day.addProps({
              text: text,
              textClass: textClass
            });
            if (diffZodiacDays < 4) {
              firstZodiacDay = zodiacs[z + 1];
              z++;
            }
            if (diffSeasonDays < 4) {
              firstSeasonDay = seasons[s + 1];
              s++;
            }
            this.days[dayOfYear] = day;
            week.push(day);
            dayOfYear++;
          }
          month.push(week);
        }
        this.months.push(month);
      }
    };
    this.populateData();
    this.setYear = function(year) {
      this.year = year;
      this.setupYearProperties();
      this.populateData();
    };
  }

}
