# life-calendar

A calendar that's the same each year and doesn't need day numbers. Leap weeks based on the open source, public domain [Symmetry454 Calendar arithmetic by Dr. Irv Bromberg](https://individual.utoronto.ca/kalendis/symmetry.htm).

## How does it work

- Every year starts on a Monday and ends on a Sunday. To achieve this, instead of a leap day every four years, there is a leap week every 5-6 years at the end of December. Leap years are exactly 53 weeks long, other years are 52 weeks long.
- Every season is exactly 13 weeks long, so every season also starts on a Monday and ends on a Sunday. On leap years, Autumn is 14 weeks long because of the leap week.
- Each month is either four weeks long or five weeks long. Each season is made up three months, with the middle month of the season being the five-week month, the other months are each four weeks long.
- Since each month is an even number of weeks, date names do not require day numbers. So, rather than naming the first day of the year "January 1", it can be named "January's 1st Monday" or "the first Monday of January." No more wondering what day of the week some date is, or looking up the date for next Wednesday.

## Developing

Once you've created a project and installed dependencies with `npm install` (or `pnpm install` or `yarn`), start a development server:

```bash
npm run dev

# or start the server and open the app in a new browser tab
npm run dev -- --open
```

## Building

Before creating a production version of your app, install an [adapter](https://kit.svelte.dev/docs#adapters) for your target environment. Then:

```bash
npm run build
```

> You can preview the built app with `npm run preview`, regardless of whether you installed an adapter. This should _not_ be used to serve your app in production.
